package com.userauth.userauth.controller;

import com.userauth.userauth.dto.UserDTO;
import com.userauth.userauth.entity.User;
import com.userauth.userauth.repository.UserRepository;
import com.userauth.userauth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public User registerUser(@RequestBody User user){
        return userService.registerUser(user);
    }

    @GetMapping
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @GetMapping("/login/{email}/{password}")
    public Object login(@PathVariable String email, @PathVariable String password) {
        return userService.getUserByEmailPassword(email, password);
    }

    @GetMapping("/find")
    public Object findUserByEmail(@RequestParam String email) {
        return userService.getUserByEmail(email);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable int id) {
        userService.deleteById(id);
    }

    @PostMapping("/resetRequest")
    public String handleRequest(@RequestParam("email") String email) throws MessagingException, IOException {
        userService.passwordChangeRequest(email);
        return "A token is sent to your email to ";
    }

    @PutMapping("/changePassword/{email}/{token}")
    public Object changePassword(@PathVariable("email") String email, @PathVariable("token") String token, @RequestParam("password") String password) {
        return userService.resetPassword(email, token, password);
    }
}
