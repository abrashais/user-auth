package com.userauth.userauth.dto;

import com.userauth.userauth.entity.User;
import lombok.Value;

@Value
public class UserDTO {

    int id;
    String firstName;
    String lastName;
    String email;
    public UserDTO toUserDTO(User user) {
        return new UserDTO(user.getId(),
                user.getFirstName(),
                user.getFirstName(),
                user.getEmail());
    }
}
