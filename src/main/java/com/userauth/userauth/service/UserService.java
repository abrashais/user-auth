package com.userauth.userauth.service;

import com.userauth.userauth.dto.UserDTO;
import com.userauth.userauth.entity.ResetRequest;
import com.userauth.userauth.entity.User;
import com.userauth.userauth.helper.EmailHelper;
import com.userauth.userauth.repository.ResetRequestRepository;
import com.userauth.userauth.repository.UserRepository;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmailHelper emailHelper;

    @Autowired
    private ResetRequestService resetRequestService;

    @Autowired
    private ResetRequestRepository resetRequestRepository;

    public User registerUser(User user) {
        boolean userExists = userRepository.findByEmail(user.getEmail())
                .isPresent();

        if(userExists) {
            throw new IllegalStateException("email already taken!");
        } else {
            return userRepository.save(user);
        }
    }

    public Object getUserByEmailPassword(String email, String password) {
        User wantedUser = userRepository.getUserByEmailAndPassword(email, password);
        if(wantedUser != null) {
            return wantedUser;
        }
        return "User doesn't exist!";
    }

    public Object getUserByEmail(String email) {
        if(userRepository.findByEmail(email).isPresent()) {
            User user = userRepository.findByEmail(email).get();
            return toUserDTO(user);
        }
        return "User could not be found!";
    }

    public void deleteById(int id) {
        userRepository.deleteById(id);
    }

    public void passwordChangeRequest(String email) throws MessagingException, IOException {

        User user = userRepository.findByEmail(email).get();
        boolean emailExists = userRepository.findByEmail(email)
                .isPresent();
        String token = UUID.randomUUID().toString();
        ResetRequest createdRequest = new ResetRequest(token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(30), false);
        user.getRequests().add(createdRequest);
        resetRequestService.saveRequest(createdRequest);

        if (emailExists) {
            emailHelper.sendEmail(email, token);
        }
    }

    public String  resetPassword(String email, String token, String password) {
        if(resetRequestService.validateTimeAndStatus(token)) {
            ResetRequest modified = resetRequestRepository.findByToken(token).get();
            modified.setRequestServed(true);
            resetRequestRepository.save(modified);

            User user = userRepository.findByEmail(email).get();
            user.setPassword(password);
            userRepository.save(user);

            return "The password is successfully changed!";
        } else {
            return "Some thing went wrong. The reason could be that the token might has Expired OR Used!";
        }
    }

    public Object getUserByEmail(String email, String token, String password) {
        User user = userRepository.findByEmail(email).get();
        user.setPassword(password);
        userRepository.save(user);
        return user.getPassword();
    }

    public UserDTO toUserDTO(User user) {
        return new UserDTO(user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail());
    }

}
