package com.userauth.userauth.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ResetRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int requestId;

    private String token;
    private LocalDateTime createdAt;
    private LocalDateTime expiresAt;
    private boolean requestServed;

    public ResetRequest(String token,
                        LocalDateTime createdAt,
                        LocalDateTime expiresAt,
                        boolean requestServed) {
        this.token = token;
        this.createdAt = createdAt;
        this.expiresAt = expiresAt;
        this.requestServed = requestServed;
    }
}
