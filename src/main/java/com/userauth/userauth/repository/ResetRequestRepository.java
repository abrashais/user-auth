package com.userauth.userauth.repository;

import com.userauth.userauth.entity.ResetRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ResetRequestRepository extends JpaRepository<ResetRequest, Integer> {
    Optional<ResetRequest> findByToken(String token);
}
