package com.userauth.userauth.repository;

import com.userauth.userauth.dto.UserDTO;
import com.userauth.userauth.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    User getUserByEmailAndPassword(String email, String password);

    UserDTO findUserByFirstName(String firstName);

    Optional<User> findByEmail(String email);
}
